
$(function(){
    $(".billing").on("click", function(){
        $("#hidden-fields").stop().slideToggle();
    });
    $(window).load(function(){
        var bar = $('#hidden-banner');

        if(bar.css('top') <= '-1px' ){
            bar.delay(1500).slideDown(300);
        }
    });

    $('.check, .exclusive').css( 'opacity' , '1');
    $('.badge-icon').css({'opacity' : '1', 'top' : '0px'});

    $('#downsell').delay(4500).fadeIn();

    $('#downsell .close-img').stop().on('click', function(){
      $('#downsell').fadeOut();
    });

    var position1 = $('#top').offset();
    var step      = $('.step');
    var position2 = $('#section-2').offset();
    var benefit   = $('.benefit');
    var position4 = $('#ooter').offset();
    console.log(position4)
    $(window).scroll(function (e) {
        var y = $(this).scrollTop();

        if(y >= position1.top + ($(this).height() * .25)){
          e.preventDefault()
            $('.step-1').css({'top': '0px', 'opacity' : '1'});
            $('.step-1 p').css({'top' : '48%'});
        }
        for( var i = 0; i < step.length; i++){
            if(y >= step.eq(i).offset().top - (($(this).height() / 1))){
              step.eq(i).addClass('animation');
          }
        }(i);

        for( var b = 0; b < benefit.length; b++){
          if(y >= benefit.eq(b).offset().top - (($(this).height() / 1))){
            e.preventDefault()

              benefit.eq(b).find('img').css({'left' : '0%', 'opacity' : '1'}).removeClass('small-width');
              benefit.eq(b).find('.sub-header').css({'left' : '0%', 'opacity' : '1'});
              benefit.eq(b).find('p').css({ 'opacity' : '1'});
          }
        }
        var $elem = $('footer');
        var $window = $(window);

        var docViewTop = $window.scrollTop();
        var docViewBottom = docViewTop + $window.height();

        var elemTop = $elem.offset().top;
        var elemBottom = elemTop + $elem.height();

        if (elemTop >= (docViewBottom + 20) || (elemTop + $('#cta').height()) >= (docViewBottom + 34) ) {
            $('#cta').css('position', 'fixed');
        } else {
            $('#cta').css({'position' : 'relative'});
        }
    });
    var visa = new RegExp("^4"),
    mastercard = new RegExp("^5");
    $('#cc_number').keyup( function(i){
      if($(this).val().match(visa)){
            $('#mastercard').stop().animate({'opacity' : '.2'});
            console.log('yes')
      }else
      if($(this).val().match(mastercard)){
            $('#visa').stop().animate({'opacity' : '.2'});
      }
      else{
          $('.card-type').stop().animate({'opacity' : '1'});
          console.log('no');
        }
    });

});
var stepOne = angular.module('stepOne', []);

stepOne.controller('ListCtrl', ['$scope', '$http',
  function ($scope, $http) {
    $http.get('content.json').success(function(data) {
      $scope.heading = data;
    });
  }]);

stepOne.controller('headerDir', ['$scope' , function($scope){
  $scope.templates =
  [ { name: '../ngIncludes/content.html', url: 'ngIncludes/content.html'}
  ];
  $scope.template = $scope.templates[0];
}]);
