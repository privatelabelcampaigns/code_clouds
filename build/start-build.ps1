<#

.SYSNOPSIS
This script builds and publihes this project to a nuget feed.

.PARAMETER Tasks
The list of tasks to invoke.

#>

[CmdletBinding()]
Param(
	[Parameter(Position=0)]
	[string[]]$Tasks = @("build"),

    [Parameter(Position=1)]
    $TemplateName,

    [switch]
    $Help
)

$rootDirectory = (Split-Path $PSScriptRoot -Parent);
$toolsDirectory = "$rootDirectory\tools";

# Import Psake module
Remove-Module [p]sake;
$psakeModule = (Get-ChildItem "$rootDirectory\tools\psake*\tools\psake.psm1").FullName | Sort-Object $_ | Select-Object -Last 1;
Import-Module $psakeModule;

# Run psake tasks
if ($ShowHelp)
{ Invoke-psake "$rootDirectory\build\tasks.ps1" -docs; }
else
{
    Invoke-psake `
    	-buildFile "$rootDirectory\build\tasks.ps1" `
    	-taskList $Tasks `
    	-framework 4.5.2 `
        -properties @{
            "Template"=$TemplateName;
        };
}
	
if(-not $psake.build_success) { exit 1; }
