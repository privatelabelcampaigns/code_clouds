Properties {
	# Paths
	$RootDirectory = (Split-Path $PSScriptRoot -Parent);
    $BinDirectory = "$RootDirectory\build\bin";
    $Template = "";
    
    $Settings = (Get-Content "$RootDirectory\build\config\settings.json" | Out-String | ConvertFrom-Json);
}

Task build -depends Uncss;

Task Init -description "" `
-action {
}

Task Uncss -description "" `
-action {
    $templates = @();
    if ([String]::IsNullOrEmpty($Template))
    { $templates = Get-ChildItem "$RootDirectory\src" -Directory -Exclude @("node_modules") | Where-Object { (-not $_.Name.StartsWith("_")) } | Select-Object -ExpandProperty FullName; }
    else
    { $templates = @("$RootDirectory\$Template"); }



    foreach ($folder in $templates)
    {
        if (Test-Path $folder -PathType Container)
        {
            echo $folder;
        }
        else
        { Write-Warning "cannot of find the '$(Split-Path $folder -Leaf)' template folder."; }
    }
}